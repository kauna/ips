@extends('adminlayout');
@section('title')
Dashboard IPS
@stop
@section('page-title')
Administrator Dashboard
@stop
@section('divider')
Dashboard
@stop
@section('row1')
<div class="span12">
    <div class="widget">
        <div class="widget-title">
            <h4><i class="icon-globe">Welcome</i></h4>
                                   <span class="tools">
                                   <a href="javascript:;" class="icon-chevron-down"></a>
                                   <a href="javascript:;" class="icon-remove"></a>
                                   </span>
        </div>
        <div class="widget-body">
            <p>
                This system provides an assignment administration and submission platform and also a lecturer-student interaction platform
            </p>
        </div>
    </div>
</div>
@stop