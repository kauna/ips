@extends('layout')
    @section('title')
         	    Dashboard IPS
    @stop
    @section('page-title')
            Dashboard
    @stop
    @section('divider')
            Dashboard
    @stop
    @section('row1')
        <div class="span12">
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-globe">Welcome</i></h4>
                                   <span class="tools">
                                   <a href="javascript:;" class="icon-chevron-down"></a>
                                   <a href="javascript:;" class="icon-remove"></a>
                                   </span>
                </div>
                <div class="widget-body">
                    <p>
                        This system provides an assignment administration and submission platform and also a lecturer-student interaction platform
                    </p>
                </div>
            </div>
        </div>
    @stop

    @section('row2')
    <div class="span12">
        <div class="widget">
            <div class="widget-title">
                <h4><i class="icon-globe">School Calendar</i></h4>
                            <span class="tools">
                                 <a href="javascript:;" class="icon-chevron-down"></a>
                                 <a href="javascript:;" class="icon-remove"></a>
                            </span>
            </div>
            <div class="widget-body">
                <!-- BEGIN CALENDAR PORTLET-->
                        <div class="row-fluid">
                            <div class="span3 responsive" data-tablet="span12 fix-margin" data-desktop="span8">
                                <!-- BEGIN DRAGGABLE EVENTS PORTLET-->
                                <h3 class="event-form-title">Draggable Events</h3>
                                <div id="external-events">
                                    <form class="inline-form">
                                        <input type="text" value="" class="span12" placeholder="Event Title..." id="event_title" /><br />
                                        <select id="event_priority" data-placeholder="Select Label..." class="span12 chosen">
                                            <option value=""></option>
                                            <option value="default">Default</option>
                                            <option value="success">Normal</option>
                                            <option value="info">Medium</option>
                                            <option value="warning">Urgent</option>
                                            <option value="important">Important</option>
                                        </select>
                                        <div class="space12"></div>
                                        <a href="javascript:;" id="event_add" class="btn">Add Event</a>
                                    </form>
                                    <hr />
                                    <div id="event_box">
                                    </div>
                                    <label for="drop-remove">
                                        <input type="checkbox" id="drop-remove" />remove after drop
                                    </label>
                                    <hr class="visible-phone"/>
                                </div>
                                <!-- END DRAGGABLE EVENTS PORTLET-->
                            </div>
                            <div class="span9">
                                <div id="calendar" class="has-toolbar"></div>
                            </div>
                        </div>
                        <!-- END CALENDAR PORTLET-->
            </div>
        </div>
    </div>
    @stop
