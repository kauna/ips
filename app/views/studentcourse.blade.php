@extends('layout')
    @section('title')
         {{$course_details->course_code}}
    @stop
    @section('page-title')
        {{$course_details->course_code}}: {{$course_details->course_title}}
    @stop
    @section('divider')
        {{$course_details->course_code}}
    @stop
        @section('row1')
        <div class="span12">
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-globe">Course Objective</i></h4>
                        <span class="tools">
                             <a href="javascript:;" class="icon-chevron-down"></a>
                             <a href="javascript:;" class="icon-remove"></a>
                        </span>
                </div>
                <div class="widget-body">
                    <p>{{$course_details->course_objective}}</p>
                </div>
            </div>
        </div>
        @stop


        @section('row2')
        <div class="span12">
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-globe"> Course Materials  </i></h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <a href="javascript:;" class="icon-remove"></a>
                        </span>
                </div>
                <div class="widget-body">
                    <ol>
                    <li>Industrial Technological Economics
                            <button type="submit" class="btn">Download</button></li>
                        <li>Course Outline
                            <button type="submit" class="btn">Download</button></li>
                    </ol>
                </div>
            </div>
        </div>
        @stop

        @section('row3')
        <div class="span12">
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-globe"> Assignment(s)</i></h4>
                                <span class="tools">
                                   <a href="javascript:;" class="icon-chevron-down"></a>
                                   <a href="javascript:;" class="icon-remove"></a>
                                </span>
                </div>
                <div class="widget-body">
                    <ul>
                    <p>Assignment 1<p>
                    <button type="submit" class="btn">Download Assignment</button>
                    <div class="controls">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="icon-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                                       <span class="btn btn-file">
                                       <span class="fileupload-new">Upload assignment file</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" class="default" />
                                       </span>
                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>
                        <div class="control-group">
                            <div class="controls">
                                <textarea class="span12" rows="3"></textarea>
                            </div>
                        </div>
                    <button type="submit" class="btn">Submit</button>
                  </ul>
                </div>
            </div>
        </div>
        @stop


        @section('row4')
        <div class="span12">
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-globe">Forums</i></h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <a href="javascript:;" class="icon-remove"></a>
                        </span>
                </div>
                <div class="widget-body">
                        <ul class="chats normal-chat">
                             <li class="in">
                                 <img class="arrow" alt="" src="img/avartar1.jpg" />
                                     <div class="message ">
                                        <span class="arrow"></span>
                                            <a href="#" class="name">Jide</a>
                                        <span class="datetime">at Apr 14, 2015 3:23</span>
                                        <span class="body">
                                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                        </span>
                                      </div>
                             </li>
                             <li class="out">
                                <img class="arrow" alt="" src="img/avartar2.jpg" />
                                <div class="message ">
                                    <span class="arrow"></span>
                                    <a href="#" class="name">Joshua</a>
                                    <span class="datetime">at Apr 14, 2013 3:23</span>
                                        <span class="body">
                                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                        </span>
                                </div>
                             </li>
                                <li class="in">
                                <img class="arrow" alt="" src="img/avartar2.jpg" />
                                <div class="message ">
                                    <span class="arrow"></span>
                                    <a href="#" class="name">Ife</a>
                                    <span class="datetime">at Apr 14, 2013 3:23</span>
                                        <span class="body">
                                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                        </span>
                                </div>
                             </li>
                        </ul>
                    <div class="chat-form">
                        <div class="input-cont">
                            <input type="text" placeholder="Type a message here..." />
                        </div>
                        <div class="btn-cont">
                            <a href="javascript:;" class="btn btn-primary">Send</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop
