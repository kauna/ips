@extends('adminlayout')
@section('title')
Admin Page
@stop

@section('page-title')
User Operations
@stop

@section('divider')
Administrator
@stop

@section('row1')
<div class="span12">
    <div class="widget">
        <div class="widget-title">
            <h4><i class="icon-globe">Add Users and Assign Courses</i></h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <a href="javascript:;" class="icon-remove"></a>
                        </span>
        </div>
        <div class="widget-body">
            <!-- BEGIN FORM-->
            <form action="{{ action('AdminController@handleAddUsers') }}" method="post" class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">Username</label>
                    <div class="controls">
                        <input name="username" type="text" class="span6 " />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Password</label>
                    <div class="controls">
                        <input name="password" type="password" class="span6">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">First name</label>
                    <div class="controls">
                        <input name="firstname" type="text" class="span6 " />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Last name</label>
                    <div class="controls">
                        <input name="lastname" type="text" class="span6 " />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">User type</label>
                    <div class="controls">
                        <input name="user_type" type="text" class="span6 " />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Assign Courses</label>
                    <div class="controls">
                        @foreach($courses as $course)
                        <label class="checkbox line">
                            <input type="checkbox" value="" /> {{$course->course_code}}
                        </label>
                        @endforeach
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="reset" class="btn">Cancel</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
@stop


@section('row2')
<div class="span12">
    <div class="widget">
        <div class="widget-title">
            <h4><i class="icon-globe">Update Users and Delete</i></h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <a href="javascript:;" class="icon-remove"></a>
                        </span>
        </div>
        <div class="widget-body">
            @if ($users->isEmpty())
            <p>There are no users</p>
            @else
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User name</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>User type</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->user_id}}</td>
                    <td>{{ $user->username}}</td>
                    <td>{{ $user->firstname}}</td>
                    <td>{{ $user->lastname}}</td>
                    <td>{{ $user->user_type}}</td>
                    <td>
                        <a href="{{ action('AdminController@userEdit', $id)}\}"
                           class = "btn btn-default">Edit</a>
                        <a href="{{ action('AdminController@CourseDelete', id)}\}"
                           class = "btn btn-danger">Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>
@stop