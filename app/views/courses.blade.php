@extends('adminlayout')
@section('title')
Admin Page
@stop

@section('page-title')
Course Operations
@stop

@section('divider')
Administrator
@stop

@section('row1')
<div class="span12">
    <div class="widget">
        <div class="widget-title">
            <h4><i class="icon-globe">Add Courses</i></h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <a href="javascript:;" class="icon-remove"></a>
                        </span>
        </div>
        <div class="widget-body">
            <!-- BEGIN FORM-->
            <form action="{{ action('AdminController@handleAddCourses') }}" method="post" class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">Course Code</label>
                    <div class="controls">
                        <input name="course-code" type="text" class="span6 " />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Course Title</label>
                    <div class="controls">
                        <input name="course-title" type="text" class="span6 " />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Course Objective</label>
                    <div class="controls">
                        <textarea name="course-objective" class="span6" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="reset" class="btn">Cancel</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
@stop


@section('row2')
<div class="span12">
    <div class="widget">
        <div class="widget-title">
            <h4><i class="icon-globe">Update Courses and Delete</i></h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <a href="javascript:;" class="icon-remove"></a>
                        </span>
        </div>
        <div class="widget-body">
            @if ($courses->isEmpty())
            <p>There are no courses</p>
            @else
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Course Code</th>
                    <th>Course Title</th>
                    <th>Course Objective</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)
                <tr>
                    <td>{{ $course->id}}</td>
                    <td>{{ $course->course_code}}</td>
                    <td>{{ $course->course_title}}</td>
                    <td>{{ $course->course_objective}}</td>
                    <td>
                        <a href="{{ action('AdminController@courseEdit', $id)}\}"
                           class = "btn btn-default">Edit</a>
                        <a href="{{ action('AdminController@courseDelete', id)}\}"
                           class = "btn btn-danger">Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>
@stop