<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('login');
});

// Handles the login post request
Route::post('login', 'HomeController@login');

//Handles dashboard request
Route::get('dashboard', 'DashboardController@index');

// Handles admin request
Route::get('admin', 'AdminController@admin');

// Handles admincourse request
Route::get('courses', 'AdminController@courses');

// Handles addcourses request
Route::post('courses', 'AdminController@handleAddCourses');

//Handles studentcourse request
Route::get('/course/{id}', 'CourseController@getCourse');

// Handles adminuser request
Route::get('users', 'AdminController@users');

// Handles addusers request
Route::post('users', 'AdminController@handleAddUsers');

//Handles layout request
Route::get('layout', 'DashboardController@layout');

// Handles logout request
Route::get('logout', 'HomeController@logout');

Route::get('hallo', function(){
   return View::make('hallo');
});






