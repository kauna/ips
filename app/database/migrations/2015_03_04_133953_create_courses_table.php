<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Create table
        Schema::create('courses', function($table){

            $table->increments('id');
            $table->string('course_code');
            $table->string('course_title');
            $table->string('course_objective');
            $table->integer('status')->default('1');
            $table->timestamps();
        });

    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Drop table
        Schema::drop('courses');
	}

}
