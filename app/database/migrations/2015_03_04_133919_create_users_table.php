<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Schema to create
        Schema::create('users', function($table){

            $table->increments('user_id');
            $table->string('username');
            $table->string('password');
            $table->rememberToken();
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('user_type');
            $table->integer('status')->default('1');
            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Drop table
        Schema::drop('users');
	}

}
