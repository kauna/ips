<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        //Schema to create
        Schema::create('course_materials', function($table){

            $table->increments('id');
            $table->string('course_material_title');
            $table->string('course_material_directory');
            $table->integer('uploaded_by');
            $table->foreign('uploaded_by')->references('user_id')->on('users');
            $table->integer('status');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Drop table
        Schema::drop('course_materials');
	}

}
