<?php
/**
 * Created by PhpStorm.
 * User: IFEIBIRONKE
 * Date: 01/05/15
 * Time: 10:51
 */

class AdminController extends BaseController{

    public function admin(){
        //Opens the dashboard after user login
//        echo "hello";
        if(Session::has('user')){
            $user_id = Session::get('user')->user_id;
//            $courses = DB::table('courses')->get();
//            var_dump($courses);
            return View::make('admin');
        }else{
            return Redirect::to('/');
        }

    }

    public function courses(){
        $courses = Course::all();
//        $courses = DB::table('courses')->get();
//        var_dump($courses);
//        return View::make('courses')->with('courses', $courses);
        return View::make('courses', compact('courses'));
    }

    public function users(){
        $users = User::all();
        $courses = Course::all();
//        var_dump($users);
        return View::make('users', compact('users'), compact('courses'));
    }


    public function handleAddUsers(){
        // Handle add users
//        $courses = DB::table('courses')->get();
//        var_dump($courses);
        $user = new User;
        $user->user_type = Input::get('user-type');
        $user->username = Input::get('username');
        $user->password = Input::get('password');
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->save();
        return View::make('users');
    }

    public function handleAddCourses(){
        // Handle add courses
        $course = new Course;
        $course->course_code = Input::get('course-code');
        $course->course_title = Input::get('course-title');
        $course->course_objective = Input::get('course-objective');
        $course->save();
        return View::make('courses');
    }

    public function courseEdit(){
    }

    public function courseDelete(){
    }

    public function userEdit(){
    }

    public function userDelete(){
    }

} 