<?php
/**
 * Created by PhpStorm.
 * User: Babajide Owosakin
 * Date: 4/11/2015
 * Time: 12:12 PM
 */

class DashboardController extends BaseController {

//    public function __construct(){
//        parent::__construct();
//        $this->beforeFilter('crsf', array('on' => 'post'));
//        $this->beforeFilter('auth', array('on' => array('index')));
//    }

    public function index(){
        //Opens the dashboard after user login
//        echo "hello";
        if(Session::has('user')){
//            echo "hello";
            //Get user id from session
            $user_id = Session::get('user')->user_id;
            //Fetch courses from DB
//            $courses = DB::table('courses')->join('user_course', 'user_course.user_id', '=', 'courses.course_id')->get();
            $courses = DB::table('courses')->join('user_course', 'user_course.course_id', '=', 'courses.id')->where('user_course.user_id', '=', $user_id)->get();
//            var_dump($courses);
            return View::make('dashboard')->with('courses', $courses);
        }else{
            return Redirect::to('/');
        }

    }

    public function layout(){
        //Opens the dashboard after user login
//        echo "hello";
        if(Session::has('user')){
//            echo "hello";
            return View::make('layout');
        }else{
            return Redirect::to('/');
        }

    }
//
//    public function course($course_id){
//        if(Session::has('user')){
////            echo "hello";
//            return View::make('/course/');
//        }else{
//            return Redirect::to('/');
//        }

//    }

} 